package test;

/**
 * Created by Arman on 07/06/2016.
 */
public class BBTrump extends test.Trump{

    public enum TrumpType {
        NULL,
        NO_TRUMP,
        STANDART,
        CAPOT
    };

    public static final BBTrump NO_TRUMP = new BBTrump(Suit.NO_SUIT, -1);
    public static final BBTrump All_TRUMP = new BBTrump(Suit.NO_SUIT, -2);

    private boolean contra;
    private boolean re_contra;
    private Team team = null;
    private boolean pass;
    private boolean capot;

    public BBTrump() {

        super(Suit.NO_SUIT, 0);

        pass = false;
        capot = false;
        contra = false;
        re_contra = false;
    }

    public BBTrump(Suit suit, int value) {

        super(suit, value);
        pass = false;
        capot = false;
        contra = false;
        re_contra = false;
    }

    public BBTrump(Suit suit, int value, Team team) {

        this(suit, value);
        this.team = team;
    }

    public boolean isNoTrump() {
        return getSuit() == Suit.NO_SUIT && getValue() == -1;
    }

    public boolean isAllTrump() {
        return getSuit() == Suit.NO_SUIT && getValue() == -2;
    }

    public boolean getPass() {
        return pass;
    }

    public void setPass(boolean pass) {

        this.pass = pass;
    }

    public boolean getCapot() {
        return capot;
    }

    public void setCapot(boolean capot) {

        this.capot = capot;
    }

    public boolean getContra() {
        return contra;
    }

    public void setContra(boolean contra) {
        this.contra = contra;
    }

    public boolean getReContra() {
        return re_contra;
    }

    public void setReContra(boolean re_contra) {
        this.re_contra = re_contra;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public BBTrump clone() {

        return BBTrump.clone(this);
    }

    public static BBTrump clone(BBTrump trump) {

        BBTrump clone = new BBTrump(trump.getSuit(), trump.getValue());

        clone.setPass(trump.getPass());
        clone.setCapot(trump.getCapot());
        clone.setContra(trump.getContra());
        clone.setReContra(trump.getReContra());

        return clone;
    }
}
