package test;

/**
 * Created by Arman on 29-Apr-16.
 */
public enum Rank {

    JOKER(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(11), QUEEN(12), KING(13), ACE(14);

    private int value;

    Rank(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Rank valueOf(int value) {
        Rank result = Rank.ACE;
        switch (value) {
            case 1:
                result = Rank.JOKER;
                break;
            case 2:
                result = Rank.TWO;
                break;
            case 3:
                result = Rank.THREE;
                break;
            case 4:
                result = Rank.FOUR;
                break;
            case 5:
                result = Rank.FIVE;
                break;
            case 6:
                result = Rank.SIX;
                break;
            case 7:
                result = Rank.SEVEN;
                break;
            case 8:
                result = Rank.EIGHT;
                break;
            case 9:
                result = Rank.NINE;
                break;
            case 10:
                result = Rank.TEN;
                break;
            case 11:
                result = Rank.JACK;
                break;
            case 12:
                result = Rank.QUEEN;
                break;
            case 13:
                result = Rank.KING;
                break;
            case 14:
                result = Rank.ACE;
                break;
        }
        return result;
    }
}
