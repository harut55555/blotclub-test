package test;

import test.*;
//import am.blotclub.core.entities.Card;
//import am.blotclub.core.entities.Color;
//import am.blotclub.core.entities.Rank;

/**
 * Created by Arman on 07-Jun-16.
 */
public class BBCard extends test.Card {

    protected BBTrump.TrumpType trumpType;

    private boolean allowed;

    private boolean isUsed;

    public BBCard(Suit s, Rank r) {

        this(s, (s == Suit.CLUBS || s == Suit.SPADES) ? Color.BLACK : Color.RED, r);
    }

    public BBCard(Suit s, Color c, Rank r) {
        super(s, c, r);
        trumpType = BBTrump.TrumpType.NULL;
    }

    public boolean isTrump() {
        return trumpType == BBTrump.TrumpType.STANDART ? true : false;
    }

    public void setTrumpType(BBTrump.TrumpType type) {
        this.trumpType = type;
    }

    public boolean getAllowed() {
        return this.allowed;
    }

    public void setAllowed(boolean value) {
        this.allowed = value;
    }

    public BBTrump.TrumpType getTrumpType() {
        return trumpType;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public int getWeightT() {

        int result = 0;
        if(this.trumpType == BBTrump.TrumpType.NULL || this.trumpType == BBTrump.TrumpType.NO_TRUMP) {
            switch (this.getRank()) {
                default:
                case TWO:
                case THREE:
                case FOUR:
                case FIVE:
                case SIX:
                case JOKER:
                    result = 0;
                    //throw new Exception("Unsupported Suit type");
                    break;
                case SEVEN:
                    result = 1;
                    break;
                case EIGHT:
                    result = 2;
                    break;
                case NINE:
                    result = 3;
                    break;
                case TEN:
                    result = 7;
                    break;
                case JACK:
                    result = 4;
                    break;
                case QUEEN:
                    result = 5;
                    break;
                case KING:
                    result = 6;
                    break;
                case ACE:
                    result = 8;
                    break;
            }
        }
        else if(this.trumpType == BBTrump.TrumpType.STANDART){
            switch (this.getRank()) {
                default:
                case TWO:
                case THREE:
                case FOUR:
                case FIVE:
                case SIX:
                case JOKER:
                    result = 0;
                    //throw new Exception("Unsupported Suit type");
                    break;
                case SEVEN:
                    result = 9;
                    break;
                case EIGHT:
                    result = 10;
                    break;
                case NINE:
                    result = 15;
                    break;
                case TEN:
                    result = 13;
                    break;
                case JACK:
                    result = 16;
                    break;
                case QUEEN:
                    result = 11;
                    break;
                case KING:
                    result = 12;
                    break;
                case ACE:
                    result = 14;
                    break;
            }
        }

        return result;
    }

    public int getWeight(BBTrump trump) {

        int result = 0;
        if(trump == BBTrump.NO_TRUMP) {
            switch (this.getRank()) {
                default:
                case TWO:
                case THREE:
                case FOUR:
                case FIVE:
                case SIX:
                case JOKER:
                    //throw new Exception("Unsupported Suit type");
                    break;
                case SEVEN:
                case EIGHT:
                case NINE:
                    result = 0;
                    break;
                case TEN:
                    result = 10;
                    break;
                case JACK:
                    result = 2;
                    break;
                case QUEEN:
                    result = 3;
                    break;
                case KING:
                    result = 4;
                    break;
                case ACE:
                    result = 19;
                    break;
            }
        }
        else if(trump.getSuit() == this.getSuit()){
            switch (this.getRank()) {
                default:
                case TWO:
                case THREE:
                case FOUR:
                case FIVE:
                case SIX:
                case JOKER:
                    //throw new Exception("Unsupported Suit type");
                    break;
                case SEVEN:
                case EIGHT:
                    result = 0;
                    break;
                case NINE:
                    result = 14;
                    break;
                case TEN:
                    result = 10;
                    break;
                case JACK:
                    result = 20;
                    break;
                case QUEEN:
                    result = 3;
                    break;
                case KING:
                    result = 4;
                    break;
                case ACE:
                    result = 11;
                    break;
            }
        }
        else {
            switch (this.getRank()) {
                default:
                case TWO:
                case THREE:
                case FOUR:
                case FIVE:
                case SIX:
                case JOKER:
                    //throw new Exception("Unsupported Suit type");
                    break;
                case SEVEN:
                case EIGHT:
                case NINE:
                    result = 0;
                    break;
                case TEN:
                    result = 10;
                    break;
                case JACK:
                    result = 2;
                    break;
                case QUEEN:
                    result = 3;
                    break;
                case KING:
                    result = 4;
                    break;
                case ACE:
                    result = 11;
                    break;
            }
        }

        return result;
    }

    @Override
    public int getWeight() {
        return getWeight(null);
    }

    @Override
    public int compareTo(Card o) {

        if(this.getWeightT() == o.getWeightT()) {
            return 0;
        }

        if(this.getWeightT() > o.getWeightT()) {
            return 1;
        }
        else {
            return -1;
        }
    }

}
