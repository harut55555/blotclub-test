package test;

/**
 * Created by Arman on 07/06/2016.
 */
public enum Team {

    TEAM_EVEN(0),
    TEAM_ODD(1);

    private int value;

    private Team(int value) {
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    public static Team valueOf(int value) {
        Team result = TEAM_EVEN;
        switch (value % 2) {
            case 0:
                result = TEAM_EVEN;
                break;
            case 1:
                result = TEAM_ODD;
                break;
        }
        return result;
    }
}
