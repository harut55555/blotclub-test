package test;

import java.util.List;


/**
 * Created by harut55555 on 6/14/16.
 */
public class Validation {

    // @params Params (stack(player, trump), pair(player, trump))
    // @return BOOLEAN (validation passed TRUE)
    public boolean checkBazar(List<Pair<Player, Trump>> biddingHistroy, Pair<Player, Trump> currentBid) {

        boolean answer = false;

        Player currentPlayer = currentBid.first;
        Trump currentTrump = currentBid.second;

        // Check
        int biddingHistorSize = biddingHistroy.size();
        if (biddingHistorSize > 0) {
            Pair lastPair = biddingHistroy.get(biddingHistorSize - 1);
            Trump lastTrump = (Trump)lastPair.second;
            Player lastPlayer = (Player)lastPair.first;
            int mathAbs = Math.abs(currentPlayer.getValue() - lastPlayer.getValue());
            boolean lastIsTeamMate = mathAbs == 2;

            int curIndex = biddingHistorSize - 1;
            int iteration = 0;
            do {
                Pair tempPair = biddingHistroy.get(curIndex);
                Player tempPlayer = (Player)tempPair.first;
                Trump tempTrump = (Trump)tempPair.second;

                mathAbs = Math.abs(currentPlayer.getValue() - tempPlayer.getValue());
                boolean teamMate = mathAbs == 2;

                if (currentTrump.getPass()) {
                    answer = true;
                    break;
                }
                else if (currentTrump.getReContra()) {
                    answer = !tempTrump.getReContra() && tempTrump.getContra() && !teamMate;
                    if (answer) break;
                }
                else if (currentTrump.getContra()) {
                    answer = !tempTrump.getReContra() && !tempTrump.getContra() && !lastTrump.getPass() && !lastIsTeamMate;
                    if (answer) break;
                }
                else {
                    answer = tempTrump.getPass() || !tempTrump.getReContra() && !tempTrump.getContra() && currentTrump.getValue() > tempTrump.getValue() && currentTrump.getValue() > 8 && tempTrump.getCapot() == currentTrump.getCapot();
                    if (!answer) break;
                }
                curIndex--;
                iteration++;
            } while (curIndex >= 0 && iteration <= 3);
        } else {
            answer = currentTrump.getValue() > 8 && !currentTrump.getReContra() && !currentTrump.getContra();
        }

        return answer;
    }

    // @params Params (List(player, curPlayer))
    // @return BazarEntity
    public BazarEntity getBazar(List<Pair<Player, Trump>> biddingHistroy, Player currentPlayer) {

        BazarEntity bazar = new BazarEntity();

        int biddingHistorSize = biddingHistroy.size();

        if (biddingHistorSize > 0) {
            Pair lastPair = biddingHistroy.get(biddingHistorSize - 1);
            Trump lastTrump = (Trump)lastPair.second;
            Player lastPlayer = (Player)lastPair.first;

            int mathAbs = Math.abs(currentPlayer.getValue() - lastPlayer.getValue());
            boolean lastIsTeamMate = mathAbs == 2;

            int curIndex = biddingHistorSize - 1;
            int iteration = 0;
            bazar.setContra(true);
            boolean isThereRecontra = false;
            boolean isThereContra = false;
            bazar.setValue(lastTrump.getValue() + 1);

            do {
                Pair tempPair = biddingHistroy.get(curIndex);
                Player tempPlayer = (Player)tempPair.first;
                Trump tempTrump = (Trump)tempPair.second;

                mathAbs = Math.abs(currentPlayer.getValue() - tempPlayer.getValue());
                boolean teamMate = mathAbs == 2;
                if (tempTrump.getReContra()) {
                    bazar.setRecontra(false);
                    bazar.setContra(false);
                    bazar.setSuit(false);
                    bazar.setCapot(false);
                    isThereRecontra = true;
                }
                if (tempTrump.getContra()) {
                    bazar.setSuit(false);
                    bazar.setContra(false);
                    bazar.setCapot(false);
                    if (!isThereRecontra) bazar.setRecontra(true);
                    if (teamMate) bazar.setRecontra(false);
                    isThereContra = true;
                }
                if (tempTrump.getCapot()) {
                    bazar.setSuit(false);
                    if (teamMate) bazar.setContra(false);
                    if (isThereContra) bazar.setCapot(false);
                }

                curIndex--;
                iteration++;
            } while (curIndex >= 0 && iteration <= 3);
        }

        return bazar;
    }
}
