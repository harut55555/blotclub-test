package test;

/**
 * Created by Arman on 07/06/2016.
 */
public class BazarEntity {

    private boolean canTalk;
    private boolean newGame;
    private int value;
    private boolean contra;
    private boolean recontra;
    private boolean capot;
    private boolean forceCapot;
    private boolean pass;
    private BBTrump trump;

    public BazarEntity() {
        this.value = 8;
        this.forceCapot = false;
        this.contra = true;
        this.recontra = false;
        this.capot = true;
        this.pass = true;
        this.canTalk = false;
        this.newGame = false;
        this.trump = new BBTrump();
    }

    public int getValue() { return this.value; }
    public boolean getContra() { return this.contra; }
    public boolean getRecontra() { return this.recontra; }
    public boolean getCapot() { return this.capot; }
    public boolean getForceCapot() { return this.forceCapot; }
    public boolean getPass() { return this.pass; }
    public boolean getCanTalk() { return this.canTalk; }
    public boolean getNewGame() { return this.newGame; }
    public BBTrump getTrump() { return this.trump; }

    public void setValue(int value) { this.value = value; }
    public void setContra(boolean value) { this.contra = value; }
    public void setRecontra(boolean value) { this.recontra = value; }
    public void setCapot(boolean value) { this.capot = value; }
    public void setForceCapot(boolean value) { this.forceCapot = value; }
    public void setPass(boolean value) { this.pass = value; }
    public void setCanTalk(boolean value) { this.canTalk = value; }
    public void setNewGame(boolean value) { this.newGame = value; }
    public void setTrump(BBTrump value) { this.trump = value; }
}
