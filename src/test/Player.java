package test;

/**
 * Created by Arman on 07/06/2016.
 */
public enum Player {

    PLAYER_1(1),
    PLAYER_2(2),
    PLAYER_3(3),
    PLAYER_4(4);

    private int value;

    private Player(int v) {
        this.value = v;
    }

    public int getValue() {
        return value;
    }

    public static Player valueOf(int value) {
        Player result = PLAYER_1;
        switch (value) {
            case 1:
                result = PLAYER_1;
                break;
            case 2:
                result = PLAYER_2;
                break;
            case 3:
                result = PLAYER_3;
                break;
            case 4:
                result = PLAYER_4;
                break;
        }
        return result;
    }
}
