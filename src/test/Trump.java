package test;

/**
 * Created by Arman on 16/06/2016.
 */
public class Trump {
    private Suit suit;
    private int value;

    public Trump(Suit suit, int value) {
        this.suit = suit;
        this.value = value;
    }

    public Suit getSuit() {
        return suit;
    }

    public int getValue() {
        return value;
    }
}
