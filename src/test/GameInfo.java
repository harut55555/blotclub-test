package test;

import java.util.List;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by Arman on 07/06/2016.
 */
public class GameInfo {

    private ConcurrentMap<Player, BBCard> lastHands;
    private List<BBCard> userCards;
    private ConcurrentMap<Player, BBTrump> biddingHistory;

    public GameInfo(ConcurrentMap<Player, BBCard> lastHands,
                        List<BBCard> userCards,
                        ConcurrentMap<Player, BBTrump> biddingHistroy) {
        this.lastHands = lastHands;
        this.userCards = userCards;
        this.biddingHistory = biddingHistroy;
    }

    public ConcurrentMap<Player, BBCard> getLastHands() {
        return lastHands;
    }

    public List<BBCard> getUserCards() {
        return userCards;
    }

    public ConcurrentMap<Player, BBTrump> getBiddingHistory() {
        return biddingHistory;
    }
}
