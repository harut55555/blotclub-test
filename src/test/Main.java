package test;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Validation validTest = new Validation();

        List biddingHistory = new ArrayList();

        for (int i=8; i<=19; i++) {
            Suit curSuit = Suit.CLUBS;
            int curValue = i;

            Trump curTrump = new Trump(curSuit, curValue);
            curTrump.setCapot(true);
            Player curPlayer = Player.PLAYER_2;
            Pair curPair = Pair.of(curPlayer, curTrump);
            biddingHistory.add(curPair);
        }



        Suit curSuit = Suit.CLUBS;
        int curValue = 20;

        Trump curTrump = new Trump(curSuit, curValue);
        curTrump.setContra(true);
        Player curPlayer = Player.PLAYER_3;
        Pair curPair = Pair.of(curPlayer, curTrump);
        biddingHistory.add(curPair);


        curSuit = Suit.DIAMONDS;
        curValue = 21;

        curTrump = new Trump(curSuit, curValue);
        curTrump.setReContra(true);
        curPlayer = Player.PLAYER_4;
        curPair = Pair.of(curPlayer, curTrump);

//        boolean answer = validTest.checkBazar(biddingHistory, curPair);
        BazarEntity answer = validTest.getBazar(biddingHistory, curPlayer);
//        System.out.println(answer);
        System.out.println(answer.getValue());
        System.out.println(answer.getSuit());
        System.out.println(answer.getCapot());
        System.out.println(answer.getContra());
        System.out.println(answer.getRecontra());
    }
}
